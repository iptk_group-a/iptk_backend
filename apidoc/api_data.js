define({ "api": [
  {
    "type": "post",
    "url": "/login/:userID/:password",
    "title": "",
    "name": "login",
    "group": "Account",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "userID",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n    userID : jack\n    password : 123123123\n}",
          "type": "type"
        }
      ]
    },
    "description": "<p>check whether the name and password correct</p>",
    "version": "0.0.0",
    "filename": "routes/account.js",
    "groupTitle": "Account"
  }
] });
