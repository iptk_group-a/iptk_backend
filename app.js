var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var firebase = require("firebase/app");

// Add the Firebase products that you want to use
require("firebase/auth");
require ("firebase/database");

const firebaseConfig = {
  apiKey: "AIzaSyBLUBFtHIAT1E5qJ5_wjP78LvFk-ox29Mk",
  authDomain: "iptk-project.firebaseapp.com",
  databaseURL: "https://iptk-project-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "iptk-project",
  storageBucket: "iptk-project.appspot.com",
  messagingSenderId: "40194924337",
  appId: "1:40194924337:web:d0822a8f16fff5500eda10",
  measurementId: "G-95QZZBCQTV"
};

firebase.initializeApp(firebaseConfig);

var indexRouter = require('./routes/index');
var accountRouter = require('./routes/account');
var friendRouter = require('./routes/friend');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


// Router
app.use('/', indexRouter);
app.use('/friend', friendRouter);
app.use('/account',accountRouter);


// app.use('/:userId',(req,res)=>{
//   res.send(req.params.userId);
// });


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});




module.exports = app;


