var express = require('express');
var router = express.Router();
var firebase = require("firebase/app");

/* GET home page. */
router.get('/', function(req, res, next) {
    //res.render('index', { title: 'Express' });
    firebase.database().ref('user').set("username2");

    res.send("123")
});
  

/**
 * 
 * @api {post} /login/:userID/:password
 * @apiName login
 * @apiGroup Account
 * 
 * @apiParam  {String} userID
 * @apiParam  {String} password
 * 
 * @apiDescription check whether the name and password correct
 * 
 * @apiParamExample  {type} Request-Example:
 * {
 *     userID : jack
 *     password : 123123123
 * }
 * 
 */
router.post('/login', function(req, res, next) {   
    res.send(req.body.userID + req.body.password);
    //firebase.database().ref('user').set(username);
    //res.send(true);
});


/**
 * 
 * @api {get} /username
 * @apiName getUsername
 * @apiGroup Account
 * 
 */
router.get('/username', function(req, res, next) {   
    // check whether the name correct
    res.send("username");
});


/**
 * @api {get} /score
 * @apiName getScore
 * @apiGroup Account
 * 
 */
router.get('/score', function(req, res, next) {   
    // check whether the name correct
    res.send("250");
});


/**
 * @api {get} occupy
 * @apiName getOccupy
 * @@apiDescription get the list of the area, which the user occupied
 * @apiGroup Account
 * 
 */
router.get('/occupy', function(req, res, next) {   
    // check whether the name correct
    var occupyList = ["Darmstadt", "Eberstadt", "Frankfurt"];
    res.send(occupyList);
});


module.exports = router;