var express = require('express');
var router = express.Router();

/**
 * @api {get} /list 
 * @apiName getFriendList
 * @apiGroup friend
 * @apiVersion  major.minor.patch
 * 
 */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});



module.exports = router;
