var express = require('express');
var database = require("firebase/app").database();

var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  
  // 存
  let user = {
    id: 123,
    name: "name",
  };

  // // Firebase
  // database.ref('user/').set(user);  //存 不需要生成UID的
  // database.ref('user').push().set(user);  // 存的同时生成UID

  // // 存的同时get UID
  // let router = database.ref('user').push(); 
  // let UID = router.key;
  // router.set(user);

  // // 取
  // var result = await database.ref('user').once('value').then((result)=>{
  //   var value = result.val();
  //   return value;
  // });
  // database.ref('user').child(UID).remove();
  res.send(user);
});

//处理 “/”的 post请求
router.post('/', function(req, res, next) {
  //res.render('index', { title: 'Express' });
  let v = [123,123];
  res.send(req.body);
});


module.exports = router;
